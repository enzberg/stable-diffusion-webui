FROM nvcr.io/nvidia/pytorch:24.01-py3

RUN echo 'install_dir = "/"' > webui-user.sh && \
    echo 'venv_dir="-"' >> webui-user.sh
RUN wget -q https://raw.githubusercontent.com/AUTOMATIC1111/stable-diffusion-webui/master/webui.sh && \
    chmod +x webui.sh && \
    ./webui.sh -f can_run_as_root --exit --skip-torch-cuda-test --xformers --force-enable-xformers --reinstall-xformers

# fix opencv
RUN pip install opencv-fixer==0.2.5 && python -c "from opencv_fixer import AutoFix; AutoFix()"

# install huggingface download tools and torch support
# HF_Transfer is supposed to be faster but seems to be unreliable on larger files, so I disabled it 
#ENV HF_HUB_ENABLE_HF_TRANSFER=1
ENV HF_HUB_ENABLE_HF_TRANSFER=0
ENV HF_HOME=/workspace/stable-diffusion-webui/models/.cache/huggingface
RUN pip install 'huggingface_hub[cli,torch,hf_transfer]'

WORKDIR "/workspace/stable-diffusion-webui/"

# add option to quick set aspect ratios
RUN git clone https://github.com/alemelis/sd-webui-ar extensions/sd-webui-ar
RUN echo "Square 1:1, 1024, 1024" > extensions/sd-webui-ar/resolutions.txt && \
    echo "Portrait 4:5, 896, 1152" >> extensions/sd-webui-ar/resolutions.txt && \
    echo "Landscape 5:4, 1152, 896" >> extensions/sd-webui-ar/resolutions.txt && \
    echo "Portrait 2:3, 832, 1216" >> extensions/sd-webui-ar/resolutions.txt && \
    echo "Landscape 3:2, 1216, 832" >> extensions/sd-webui-ar/resolutions.txt && \
    echo "Display 16:9, 1344, 768" >> extensions/sd-webui-ar/resolutions.txt && \
    echo "Wide Screen 21:9, 1536, 640" >> extensions/sd-webui-ar/resolutions.txt 

# clip interrogator tool
RUN git clone https://github.com/pharmapsychotic/clip-interrogator-ext extensions/clip-interrogator

# prompt translator
RUN git clone https://github.com/ParisNeo/prompt_translator extensions/prompt_translator

# Style selector
RUN git clone https://github.com/ahgsql/StyleSelectorXL extensions/StyleSelectorXL

# default nsfw filter (full black, no feedback)
# RUN git clone https://github.com/AUTOMATIC1111/stable-diffusion-webui-nsfw-censor extensions/stable-diffusion-webui-nsfw-censor
# nsfw filter customizable, better library
RUN git clone https://github.com/w-e-w/sd-webui-nudenet-nsfw-censor extensions/nudenet-nsfw-censor
# js based quick nsfw full image blur (insecure, additional)
RUN git clone https://github.com/aria1th/sd-webui-nsfw-filter extensions/nsfw-filter

# Download Model directly into container
RUN huggingface-cli download Lykon/dreamshaper-xl-lightning DreamShaperXL_Lightning-SFW.safetensors --resume-download --local-dir /workspace/stable-diffusion-webui/models/Stable-diffusion --local-dir-use-symlinks False 

VOLUME /root/.cache
CMD ["python", "launch.py", "--listen", "--port", "7860", "--no-download-sd-model"]
